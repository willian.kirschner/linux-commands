# linux-commands

List of the best and useful linux commands and some cool shell scripts.

`iotop -aoP` I/O running

`ps -aux` Processes running

`grep "m$"` Find a character at the end of the line

`lftp -u user-name,'password' -e ls ftp.domain.com` Access and open FTP structure folders

`x-www-browser gitlab.com/willian.kirschner` Open URL on default browser

`printf 'test' | xclip -selection c` Copy to clipboard

`find . -maxdepth 1 -perm 777 -type f` Search only the current directory for all executables

`netstat -lntu` Get a list of open ports in Linux

`update-alternatives --config java` Show list of installed Java on Linux and it allows to change priority

`scp user_name@remotehost:/directory/file /path_to_store/file` Transferring files from a remote host via SSH

`ip addr show|grep inet` Show computer IP

 `ip route show`

 `ip route flush table main` Clears all ip routes
 
#### Kubernetes

`kubectl -n <namespace> exec -it <pod> sh` 

`kubectl exec -it -n <namespace> <pod> -- env`

`kubectl port-forward -n <namespace> svc/<service> 8080:8080` 

`kubectl port-forward -n <namespace> <pod> 8080:8080` 

`kubectl -n <namespace> describe deployment <service>`

`kubectl -n <namespace> annotate --overwrite deploy/<service> test=123`

`kubectl -n <namespace> delete deployment <service>`

`kubectl scale statefulset --replicas=2 -n <namespace> <service>`

`echo $(kubectl get secret -n <namespace> <service> -o jsonpath="{.xxx.yyy.zzz}" | base64 --decode)`

`kubectl top pod --all-namespaces | sort -nrk4`

`kubectl logs -n <namespace>  deployment/<service> -f --tail=5` Get logs from all pods

`kubectl rollout status deployment/<service>`

#### Java

`java -Dmanagement.endpoints.web.exposure.include=info,health,mappings -Dmanagement.endpoint.mappings.enabled=true -jar build/libs/*.jar`

#### Git

`git reset --hard` Discard all local changes to all files

`ssh-keygen -t rsa -b 4096 -C “your_email@example.com“` and `pbcopy < ~/.ssh/id_rsa.pub`  Generate private and public SSH key to use on GitHub/GitLab
